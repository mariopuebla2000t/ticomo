# Proyecto TICOMO: Una aplicación web para realizar pedidos de comida a domicilio - Angular 14.2.0 y Spring boot 2.7.4
Proyecto desarrollado entre 6 alumnos que nace de combinar los contenidos de 4 asignaturas relacionadas:  
* **[42332 - Gestión de Proyectos Software](https://guiae.uclm.es/vistaGuia/407/42332)**
* **[42330 - Procesos de Ingeniería del Software](https://guiae.uclm.es/vistaGuia/407/42330)**
* **[42331 - Calidad de Sistemas Software](https://guiae.uclm.es/vistaGuia/407/42331)**
* **[42333 - Seguridad de Sistemas Software](https://guiae.uclm.es/vistaGuia/407/42333)**   

Este proyecto se implementó en el cuarto año de ingeniería informática, de la rama de ingeniería del software. Se trata de un sistema para la gestión de pedidos de comida a domicilio. En la aplicación web se pueden registrar clientes, ryders que se encarguen de repartir la comida y restaurantes, con sus respectivos platos. También tiene un menú adaptado para las gestiones de administrador, como eliminar, modificar o añadir restaurantes, usuarios, ryders u otros administradores.  

Gestión del proyecto usando SCRUM. Planificación de tareas usando paneles y la herramienta Azure DevOps. Gestión de la configuración y control de versiones con git y github.   

## Tecnologías utilizadas

### Backend
**Lenguaje de programación**: Java 8  
**Framework**: Spring boot 2.7.4  
**Herramientas de desarrollo**: Eclipse IDE, Maven  
**Base de datos**: MongoDB  
**Testing y cobertura**: JUnit y JaCoCo. Algunas de las pruebas realizadas se diseñaron con TDD (Test Driven Development)  
**Gestión y cumplimiento de criterios de calidad**: Sonar y Sonar Lint  

### Frontend
**Lenguaje de programación**: HTML, CSS, y Typescript  
**Framework**: Angular 14.2.0  
**Herramientas de desarrollo**: Visual Studio Code  

Más información en los PDF's asociados.
